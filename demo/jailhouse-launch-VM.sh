#!/bin/bash
#Utility for creating and launching the inmate VM with Linux

#Kick the root cell GPU test application before launching the vGPU
rgx_compute_test

#Create and launch the VM
jailhouse enable /usr/share/jailhouse/cells/k3-j721e-evm.cell
jailhouse cell linux -a arm64 -d /usr/share/jailhouse/inmate-k3-j721e-evm.dtb -c "console=ttyS1,115200n8 root=/dev/mmcblk0p2 rw rootfstype=ext4 rootwait" /usr/share/jailhouse/cells/k3-j721e-evm-linux-demo.cell /boot/Image

