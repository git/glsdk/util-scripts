#!/bin/bash
#Deamon to crash kernel on a GPIO button
#Crashes kernel using sysrq
#https://www.kernel.org/doc/html/latest/admin-guide/sysrq.html

#Wait till the push button is pressed to crash the kernel
while true;
do
        evtest /dev/input/by-path/platform-gpio-keys-event | grep -m2 BTN_
        if [ $? -eq "0" ]; then
                wall "Push button pressed, Crashing the kernel..."
                msleep 100
                echo c > /proc/sysrq-trigger
        fi
        msleep 100
done &

