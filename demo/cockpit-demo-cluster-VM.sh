#!/bin/bash
#Utility to run integrated cockpit on DRA8xx devices
#This script needs to be run on the root cell (cluster VM)

monitor_ivi_vm() {
	sleep 100
	ti-heartbeat-are-you-there
}


launch_cluster() {
	pkill matrix
	pkill weston
	sleep 2
	cluster -d /dev/dri/card0 </dev/null
}
####### Start here
##################

launch_cluster 2>/dev/null  1>&2 &

/usr/share/ti/util-scripts/demo/jailhouse-launch-VM.sh

monitor_ivi_vm 2>/dev/null  1>&2 &

/usr/share/ti/util-scripts/demo/crash-VM.sh 2>/dev/null  1>&2 &

