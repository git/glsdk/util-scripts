#!/bin/bash
#Utility to run integrated cockpit on DRA8xx devices
#This script needs to be run on the inmate cell (Infotainment VM)

run_gfx_apps() {
	DEMOLIST="OpenGLESIntroducingPVRUtils OpenGLESImageBasedLighting OpenGLESDeferredShading OpenGLESParticleSystem OpenGLESIntroducingUIRenderer"
	while true;
	do
		for i in $DEMOLIST;
		do
			echo ">>>>>>>>>>>>>>>>>>>>>> Running $i...";
			timeout -s 9 12 /usr/bin/SGX/demos/Wayland/$i -width=800 -height=600
		done
	done
}

run_multimedia() {
	CLIPLIST="TearOfSteel-Short-1280x720.265"
	while true;
	do
		for i in $CLIPLIST;
		do
			gst-launch-1.0 filesrc location=/usr/share/ti/video/$i ! h265parse ! v4l2h265dec ! video/x-raw,format=NV12 ! waylandsink
		done
	done
}

####### Start here
##################

/etc/init.d/matrix-gui-2.0 stop
msleep 500
. /etc/profile

sleep 2
run_gfx_apps 2>/dev/null 1>&2 &

run_multimedia 2>/dev/null 1>&2 &

ti-heartbeat-i-am-here 2>/dev/null 1>&2 &

/usr/share/ti/util-scripts/demo/crash-VM.sh 2>/dev/null  1>&2 &

